#!/usr/bin/env python
'''
lowering down the humanoid robot

'''
import rospy
import time
import math
import numpy as np
from std_msgs.msg import String, Float32, Bool
from sensor_msgs.msg import JointState
from std_msgs.msg import Header
from std_srvs.srv import Empty

FPS =0.2
#interpolation period 0.04s
Real_Time_Factor = 1


class serviceclient(object):
    def __init__(self):
        rospy.init_node('serviceclient', anonymous=True)

        rospy.wait_for_service('/gazebo/reset_simulation')
        rospy.wait_for_service('/gazebo/reset_world')
        print("gazebo reset service available")
        self.reset_simulation = rospy.ServiceProxy('/gazebo/reset_simulation',Empty)
        self.reset_world = rospy.ServiceProxy('/gazebo/reset_world',Empty)
        self.pause_physics = rospy.ServiceProxy('/gazebo/pause_physics',Empty)
        self.unpause_physics = rospy.ServiceProxy('/gazebo/unpause_physics',Empty)

        self.sub_msg1 = JointState()
        self.sub_msg2 = JointState()
        self.rate = rospy.Rate(FPS * Real_Time_Factor)  # 10hz
        rospy.Subscriber('/whole_body_spline_effort_controller/actual_joint_state', JointState, self.callback1)
        rospy.Subscriber('/whole_body_spline_effort_controller/desired_joint_state', JointState, self.callback2)


        time.sleep(1)  # If a message is published too soon after a publisher is created, the message is lost, even if the publisher is latched.
        # Trying to publish on a publisher that isn't ready yet will simply result in nothing happening
        # Publisher.publish() should wait for the publisher to be registered with the master, then publish the message


        self.rate.sleep()


    def run(self):
        flag = 1
        t = 0.0
        while not rospy.is_shutdown():
            #print(self.sub_msg1.position)
            #print(self.sub_msg2.position)
            #self.reset_simulation() #[ERROR] [WallTime: 1508174171.318262] [0.001000] ROS time moved backwards: 5.001s
            #ROS does not support reset.
            self.pause_physics()
            self.reset_world()
            self.unpause_physics()

            #sleep for rospy duration of 3s
            d = rospy.Duration(5, 0)
            rospy.sleep(d)

    def callback1(self,data):
        self.sub_msg1 = data
        #rospy.loginfo(rospy.get_caller_id() + 'I heard %s', data.data)
    def callback2(self,data):
        self.sub_msg2 = data
        #rospy.loginfo(rospy.get_caller_id() + 'I heard %s', data.data)


if __name__ == '__main__':
    try:
        run = serviceclient()
        run.run()
    except rospy.ROSInterruptException:
        pass
