#!/usr/bin/env python
'''
Commanding hip joints to move in a sin wave

'''
import rospy
import time
import math
import numpy as np
from std_msgs.msg import String
from sensor_msgs.msg import JointState
from std_msgs.msg import Header

FPS =25
#interpolation period 0.04s
Real_Time_Factor = 1


class pubsub(object):
    def __init__(self):
        rospy.init_node('pubsub', anonymous=True)
        self.pub_msg = JointState()
        self.pub_msg.header = Header()
        self.sub_msg1 = JointState()
        self.sub_msg2 = JointState()
        self.rate = rospy.Rate(FPS * Real_Time_Factor)  # 10hz
        rospy.Subscriber('/whole_body_spline_effort_controller/actual_joint_state', JointState, self.callback1)
        rospy.Subscriber('/whole_body_spline_effort_controller/desired_joint_state', JointState, self.callback2)

        self.pub = rospy.Publisher('/whole_body_spline_effort_controller/command', JointState, queue_size=10)

        time.sleep(1)  # If a message is published too soon after a publisher is created, the message is lost, even if the publisher is latched.
        # Trying to publish on a publisher that isn't ready yet will simply result in nothing happening
        # Publisher.publish() should wait for the publisher to be registered with the master, then publish the message


        self.rate.sleep()


    def run(self):
        flag = 1
        t = 0.0
        while not rospy.is_shutdown():
            print(self.sub_msg1.position)
            print(self.sub_msg2.position)
            if t > 5.0:
                t = 0.0
            t = t + 1.0/FPS
            self.pub_msg.header.stamp = rospy.Time.now()
            self.pub_msg.name = ['leftHipYaw', 'leftHipRoll', 'leftHipPitch', 'leftKneePitch', 'leftAnklePitch',
                              'leftAnkleRoll', \
                              'rightHipYaw', 'rightHipRoll', 'rightHipPitch', 'rightKneePitch', 'rightAnklePitch',
                              'rightAnkleRoll', \
                              'torsoYaw', 'torsoPitch', 'torsoRoll', \
                              'leftShoulderPitch', 'leftShoulderRoll', 'leftShoulderYaw', 'leftElbowPitch', \
                              'rightShoulderPitch', 'rightShoulderRoll', 'rightShoulderYaw', 'rightElbowPitch']

            self.pub_msg.position = [0, 0, 0, 0, 0, 0, \
                                     0, 0, 0, 0, 0, 0, \
                                     0, 0, 0, \
                                     math.sin(2*math.pi*t/5.0), 0, 0, 0, \
                                     -math.sin(2*math.pi*t/5.0), 0, 0, 0]

            self.pub_msg.velocity = [0]
            self.pub_msg.effort = [0]
            # hello_str = "hello world %s" % rospy.get_time()
            #rospy.loginfo(self.pub_msg)
            self.pub.publish(self.pub_msg)
            self.rate.sleep()

    def callback1(self,data):
        self.sub_msg1 = data
        #rospy.loginfo(rospy.get_caller_id() + 'I heard %s', data.data)
    def callback2(self,data):
        self.sub_msg2 = data
        #rospy.loginfo(rospy.get_caller_id() + 'I heard %s', data.data)


if __name__ == '__main__':
    try:
        run = pubsub()
        run.run()
    except rospy.ROSInterruptException:
        pass
