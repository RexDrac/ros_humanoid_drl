#!/usr/bin/env python
'''
lowering down the humanoid robot

'''
import rospy
import time
import math
import numpy as np
from std_msgs.msg import String, Float32, Bool
from sensor_msgs.msg import JointState
from std_msgs.msg import Header

FPS =0.2
#interpolation period 0.04s
Real_Time_Factor = 1


class pubsub(object):
    def __init__(self):
        rospy.init_node('pubsub', anonymous=True)
        self.pub_msg1 = JointState()
        self.pub_msg1.header = Header()
        self.pub_msg2 = Float32()
        self.sub_msg1 = JointState()
        self.sub_msg2 = JointState()
        self.rate = rospy.Rate(FPS * Real_Time_Factor)  # 10hz
        rospy.Subscriber('/whole_body_effort_controller/actual_joint_state', JointState, self.callback1)
        rospy.Subscriber('/whole_body_effort_controller/desired_joint_state', JointState, self.callback2)


        self.pub1 = rospy.Publisher('/whole_body_effort_controller/command', JointState, queue_size=10)
        self.pub2 = rospy.Publisher('/valkyrie/harness/velocity', Float32, queue_size=10)
        self.pub3 = rospy.Publisher('/valkyrie/harness/detach', Bool, queue_size=10)

        time.sleep(1)  # If a message is published too soon after a publisher is created, the message is lost, even if the publisher is latched.
        # Trying to publish on a publisher that isn't ready yet will simply result in nothing happening
        # Publisher.publish() should wait for the publisher to be registered with the master, then publish the message


        self.rate.sleep()


    def run(self):
        flag = 1
        t = 0.0
        if not rospy.is_shutdown():
            print(self.sub_msg1.position)
            print(self.sub_msg2.position)

            self.pub_msg1.header.stamp = rospy.Time.now()
            self.pub_msg1.name = ['leftHipYaw', 'leftHipRoll', 'leftHipPitch', 'leftKneePitch', 'leftAnklePitch',
                              'leftAnkleRoll', \
                              'rightHipYaw', 'rightHipRoll', 'rightHipPitch', 'rightKneePitch', 'rightAnklePitch',
                              'rightAnkleRoll', \
                              'torsoYaw', 'torsoPitch', 'torsoRoll', \
                              'leftShoulderPitch', 'leftShoulderRoll', 'leftShoulderYaw', 'leftElbowPitch', \
                              'rightShoulderPitch', 'rightShoulderRoll', 'rightShoulderYaw', 'rightElbowPitch']

            self.pub_msg1.position = [0, 0, 0, 0, 0, 0, \
                                     0, 0, 0, 0, 0, 0, \
                                     0, 0, 0, \
                                     0, 0, 0, 0, \
                                     0, 0, 0, 0]

            self.pub_msg1.velocity = [0]
            self.pub_msg1.effort = [0]
            # hello_str = "hello world %s" % rospy.get_time()
            #rospy.loginfo(self.pub_msg)
            self.pub1.publish(self.pub_msg1)
            self.rate.sleep()
            self.pub2.publish(-0.15) #-0.1-0.05, robot will remain still when velocity is set to -0.1
            #sleep for rospy duration of 3s
            d = rospy.Duration(3, 0)
            rospy.sleep(d)
            self.pub2.publish(-0.0)
            d = rospy.Duration(1, 0)
            rospy.sleep(d)
            self.pub3.publish(True)

    def callback1(self,data):
        self.sub_msg1 = data
        #rospy.loginfo(rospy.get_caller_id() + 'I heard %s', data.data)
    def callback2(self,data):
        self.sub_msg2 = data
        #rospy.loginfo(rospy.get_caller_id() + 'I heard %s', data.data)


if __name__ == '__main__':
    try:
        run = pubsub()
        run.run()
    except rospy.ROSInterruptException:
        pass